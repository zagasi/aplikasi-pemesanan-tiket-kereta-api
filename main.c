#include <stdio.h>
#include <stdlib.h>

int main() {
    //-> Deklarasi variable
    int menu, menu1, tiket = 0, total = 0, hargaEks = 567, hargaBis = 345, hargaEko = 123;
    float diskonEks = 0.2, diskonBis = 0.05, diskonEko = 0;

    //-> Script utama
    home();
    printf("Menu yang dipilih : ");
    scanf("%d", &menu);
    printf("\n");

    switch(menu) {
    case 1:
        tipe();
        printf("Menu yang dipilih : ");
        scanf("%d", &menu1);
        printf("Jumlah tiket : ");
        scanf("%d", &tiket);
        if(menu1 == 1) {
            if(tiket >= 10) {
                total = (hargaEks * tiket) - (hargaEks * diskonEks);
            } else if(tiket > 5 && tiket < 10) {
                total = (hargaEks * tiket) - (hargaEks * diskonEks);
            } else if(tiket <= 5) {
                total = (hargaEks * tiket) - (hargaEks * diskonEks);
            } else {
                total = hargaEks * tiket;
            }
        } else if(menu1 == 2) {
            if(tiket >= 10) {
                total = (hargaBis * tiket) - (hargaBis * diskonBis);
            } else if(tiket > 5 && tiket < 10) {
                total = (hargaBis * tiket) - (hargaBis * diskonBis);
            } else if(tiket <= 5) {
                total = (hargaBis * tiket) - (hargaBis * diskonBis);
            } else {
                total = hargaBis * tiket;
            }
        } else if(menu1 == 3) {
            if(tiket >= 10) {
                total = (hargaEko * tiket) - (hargaEko * diskonEko);
            } else if(tiket > 5 && tiket < 10) {
                total = (hargaEko * tiket) - (hargaEko * diskonEko);
            } else if(tiket <= 5) {
                total = (hargaEko * tiket) - (hargaEko * diskonEko);
            } else {
                total = hargaEko * tiket;
            }
        } else {
            return 0;
        }
        break;
    case 2:
        tipe();
        printf("Menu yang dipilih : ");
        scanf("%d", &menu1);
        printf("Jumlah tiket : ");
        scanf("%d", &tiket);
        if(menu1 == 1) {
            if(tiket >= 10) {
                total = (hargaEks * tiket) - (hargaEks * diskonEks);
            } else if(tiket > 5 && tiket < 10) {
                total = (hargaEks * tiket) - (hargaEks * diskonEks);
            } else if(tiket <= 5) {
                total = (hargaEks * tiket) - (hargaEks * diskonEks);
            } else {
                total = hargaEks * tiket;
            }
        } else if(menu1 == 2) {
            if(tiket >= 10) {
                total = (hargaBis * tiket) - (hargaBis * diskonBis);
            } else if(tiket > 5 && tiket < 10) {
                total = (hargaBis * tiket) - (hargaBis * diskonBis);
            } else if(tiket <= 5) {
                total = (hargaBis * tiket) - (hargaBis * diskonBis);
            } else {
                total = hargaBis * tiket;
            }
        } else if(menu1 == 3) {
            if(tiket >= 10) {
                total = (hargaEko * tiket) - (hargaEko * diskonEko);
            } else if(tiket > 5 && tiket < 10) {
                total = (hargaEko * tiket) - (hargaEko * diskonEko);
            } else if(tiket <= 5) {
                total = (hargaEko * tiket) - (hargaEko * diskonEko);
            } else {
                total = hargaEko * tiket;
            }
        } else {
            return 0;
        }
        break;
    default:
        printf("Pilihan salah");
        break;
    }

    printf("Total tiket : Rp.%d,-", total);
    return 0;
}

void home() {
    printf("--- Aplikasi Pemesanan Kereta Api --- \n");
    printf("Pilih Tujuan \n");
    printf("1. Semarang -- Jogja \n");
    printf("2. Semarang -- Solo \n");
}

void tipe() {
    printf("1. Eksekutif (Rp.567)\n");
    printf("2. Bisnis (Rp.345)\n");
    printf("3. Ekonomi (Rp.123)\n");
}
